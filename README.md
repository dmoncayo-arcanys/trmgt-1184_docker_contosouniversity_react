# Contoso University: React Client (Training)

### Requirements ###
* [Download NodeJS](https://nodejs.org/en/download/).
* [Download Docker](https://www.docker.com/products/docker-desktop).
* Setup [Contoso University API and IdentityServer4](https://bitbucket.org/dmoncayo-arcanys/trmgt-1184_docker_contosouniversity_api/src/master/).

### How do I get set up? ###

* Go to folder `cd /contoso-university-react` and Run `npm i` in CMD.
* In CMD, Run `docker build -t contosouniversityreact:dev .`.
* Then Run `docker run -it -p 3000:3000 -v /${PWD}/src:/contoso-university-react/src contosouniversityreact:dev` or `winpty docker run -it -p 3000:3000 -v /${PWD}/src:/contoso-university-react/src contosouniversityreact:dev`

* Open React Client by going to `http://localhost:3000/`
