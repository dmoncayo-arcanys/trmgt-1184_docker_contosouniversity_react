import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import throttle from 'lodash/throttle';
import createSagaMiddleware from 'redux-saga';
import { PERSISTED_STATE_KEY, AUTHORITY, CLIENT_ID } from './config/settings';

import Menu from './views/containers/Menu/state';
import Student from './views/containers/Student/state';
import Course from './views/containers/Course/state';
import Instructor from './views/containers/Instructor/state';
import Department from './views/containers/Department/state';
import Error404 from './views/containers/Errors/404';

const loadState = () => {
    try {
        const serializedState = localStorage.getItem(PERSISTED_STATE_KEY);
        return null === serializedState ? undefined : JSON.parse(serializedState);
    } catch (e) { return undefined; }
};

const saveState = (state) => {
    try {
        localStorage.setItem(PERSISTED_STATE_KEY, JSON.stringify(state));
    } catch (e) { }
};

//Module Reducers
const rootReducer = combineReducers({
    student: Student.reducer,
    course: Course.reducer,
    instructor: Instructor.reducer,
    department: Department.reducer,
});

//Module Routes
export const rootRoutes = [
    ...Menu.routes,
    ...Student.routes,
    ...Course.routes,
    ...Instructor.routes,
    ...Department.routes,
    {component: Error404}
];

export const oidcConfig = {
    onSignIn: async (user) => {
        console.log(user);
        window.location.href = '/';
    },
    authority: AUTHORITY,
    clientId: CLIENT_ID,
    responseType: 'code',
    redirectUri: window.location.origin,
};

const persistedState = loadState();
const sagaMiddleware = createSagaMiddleware();

const middleware = applyMiddleware(
    thunkMiddleware,
    sagaMiddleware
);

export const store = createStore(
    rootReducer,
    persistedState,
    middleware,
);

store.subscribe(throttle(() => {
    saveState(store.getState());
}), 1000);