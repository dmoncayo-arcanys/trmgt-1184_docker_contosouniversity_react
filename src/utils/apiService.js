import axios from 'axios';
import { REST_SERVICE_URL_ROOT, AUTHORITY, CLIENT_ID } from '../config/settings';

class ApiService {

    constructor() {
        axios.defaults.baseURL = REST_SERVICE_URL_ROOT;
        
        let service = axios.create();
        service.interceptors.request.use(function (config) {
            try {
                let value = JSON.parse(sessionStorage.getItem('oidc.user:' + AUTHORITY + ':' + CLIENT_ID + ''));
                let token = value.id_token;

                config.headers.common['Authorization'] = 'Bearer ' + token;
                config.headers.common['Content-Type'] = 'application/json';
            } catch {
                console.log("Unauthorized");
            }
            return config;
        }, function (error) {
            this.handleError(error)
            return Promise.reject(error);
        });

        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    }

    handleSuccess(response) {
        return response;
    }

    handleError = (error) => {
        const status = error.response ? error.response.status : null;
        switch (status) {
            case 401:
                console.error(error);
                break;
            case 404:
                console.error(error);
                break;
            default:
                console.error(error);
                break;
        }
        return Promise.reject(status);
    }

    redirectTo = (document, path) => {
        document.location = path;
    }

    get(path, params) {
        return this.service.get(path, {params});
    }

    async patch(path, payload) {
        const response = await this.service.request({
            method: 'PATCH',
            url: path,
            responseType: 'json',
            data: payload
        });
        return response;
    }

    async put(path, payload) {
        const response = await this.service.request({
            method: 'PUT',
            url: path,
            responseType: 'json',
            data: payload
        });
        return response;
    }

    async post(path, payload, responseType = 'json') {
        const response = await this.service.request({
            method: 'POST',
            url: path,
            responseType: responseType,
            data: payload
        });
        return response;
    }

    async delete(path, payload) {
        const response = await this.service.request({
            method: 'DELETE',
            url: path,
            responseType: 'json',
            data: payload
        });
        return response;
    }
}

export default new ApiService();