import React from 'react';
import ReactDOM from 'react-dom';
import { AuthProvider } from 'oidc-react';
import { Provider } from 'react-redux';
import { 
  BrowserRouter,
  Switch
} from 'react-router-dom';
import { oidcConfig, store, rootRoutes } from './app.modules.js';
import './index.css';
import MainRoute from "./views/components/MainRoute";
import reportWebVitals from './reportWebVitals';
import './includes/bootstrap/css/bootstrap.min.css';

const Root = (props) => {

  return (
    <AuthProvider {...oidcConfig}>
      <Provider store={store}>
        <BrowserRouter initialEntries={[props.currentPath]}>
          <Switch>
            {rootRoutes.map((route, i) => <MainRoute key={i} exact {...route} /> )}
          </Switch>
        </BrowserRouter>
      </Provider>
    </AuthProvider>
  )
}

ReactDOM.render(
  <Root />,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
