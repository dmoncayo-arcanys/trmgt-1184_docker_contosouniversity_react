// Dummy json data
export const REST_SERVICE_URL_ROOT = "https://localhost:44350/";
// Authority
export const AUTHORITY = 'https://localhost:44313/';
// Client ID
export const CLIENT_ID = 'client_react';
// Persisted key 
export const PERSISTED_STATE_KEY = "ContosoUniversity.React";
// Root
export const ROOT = '/';