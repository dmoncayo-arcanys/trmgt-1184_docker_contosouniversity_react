export const LOGIN_ROUTE = '/login';

export const STUDENT_ROUTE = '/students';

export const STUDENT_CREATE_ROUTE = '/students/create';

export const STUDENT_EDIT_ROUTE = '/students/edit/:id';

export const STUDENT_DETAILS_ROUTE = '/students/details/:id';

export const STUDENT_DELETE_ROUTE = '/students/delete/:id';

export const INSTRUCTOR_ROUTE = '/instructors';

export const INSTRUCTOR_CREATE_ROUTE = '/instructors/create';

export const INSTRUCTOR_EDIT_ROUTE = '/instructors/edit/:id';

export const INSTRUCTOR_DETAILS_ROUTE = '/instructors/details/:id';

export const INSTRUCTOR_DELETE_ROUTE = '/instructors/delete/:id';

export const COURSE_ROUTE = '/courses';

export const COURSE_CREATE_ROUTE = '/courses/create';

export const COURSE_EDIT_ROUTE = '/courses/edit/:id';

export const COURSE_DETAILS_ROUTE = '/courses/details/:id';

export const COURSE_DELETE_ROUTE = '/courses/delete/:id';

export const DEPARTMENT_ROUTE = '/departments';
export const DEPARTMENT_CREATE_ROUTE = '/departments/create';
export const DEPARTMENT_EDIT_ROUTE = '/departments/edit/:id';
export const DEPARTMENT_DETAILS_ROUTE = '/departments/details/:id';
export const DEPARTMENT_DELETE_ROUTE = '/departments/delete/:id';