import React from 'react';
import { Route } from 'react-router-dom';
import MainHeader from './MainHeader';
import MainFooter from './MainFooter';

const MainRoute = (props) => {
    return(
        <div>
            <MainHeader {...props} />
            <Route 
                exact={props.exact}
                path={props.path} 
                component={props.component}
            />
            <MainFooter {...props} />
        </div>
    );
};

export default MainRoute;