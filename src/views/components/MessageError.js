import React from 'react';
import { Alert, Button } from "react-bootstrap";

const MessageError = (props) => {

    const handleSubmit = () => {
        window.location.href = '/';
    }

    return (
        props.visible?
        <Alert variant="danger">
            { props.message }
            {
                props.message === "Your session is expired. Please login."?
                <Button 
                    variant="outline-danger" 
                    size="sm" 
                    style={{float: "right", marginTop: -3}}
                    onClick={() => handleSubmit() }>
                    Refresh Session
                </Button> : null
            }
        </Alert> : null
    );


};

export default MessageError;