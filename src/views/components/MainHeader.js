import React from 'react';
import { Navbar, Nav, Container, Button } from "react-bootstrap";
import { useAuth } from 'oidc-react';

const MainHeader = (props) => {

    const auth = useAuth();

    return (
        <Navbar bg="light" variant="light" style={{ marginBottom: 30 }}>
            <Container>
                <Navbar.Brand href="/">Contoso University</Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Item>
                        <Nav.Link href="/students">Students</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="/courses">Courses</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="/instructors">Instructors</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="/departments">Departments</Nav.Link>
                    </Nav.Item>
                </Nav>
            </Container>
        </Navbar>
        
    );
};

export default MainHeader;