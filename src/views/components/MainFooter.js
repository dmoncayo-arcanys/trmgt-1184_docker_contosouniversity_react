import React from 'react';
import { Container } from "react-bootstrap";

const MainFooter = (props) => {
    return (
        <Container style={{ marginTop: 30 }}>
            <center>2021 Copyright. Contoso University. Version 1.01</center>
        </Container>
    );
};

export default MainFooter;