import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { courseOperations } from './state'
import { departmentOperations } from '../Department/state'
import MessageError from '../../components/MessageError'

class CourseEditScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            courseID: 0,
            title: "",
            departmentID: 0,
            credits: 0,
            departments: [],
        };
    }
    
    componentDidMount() {
        this.getDepartments();
        this.getCourse();
    }

    getDepartments() {
        this.props.getDepartments().then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ departments: result.response });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    getCourse() {
        const { id } = this.props.match.params;
        this.props.getCourse(id).then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ 
                        courseID: result.response.courseID,
                        title: result.response.title,
                        departmentID: result.response.departmentID,
                        credits: result.response.credits,
                    });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    
    handleSubmit = (event) => {
        event.preventDefault();
        const { id } = this.props.match.params;
        let { title, credits, departmentID } = this.state;
        this.setState({ errorVisibility: false });
        this.props.updateCourse(id, {
            courseID: id,
            title: title,
            credits: credits,
            departmentID: departmentID,
        }).then((result) => {
            console.log("result ", result);
            if(result === 400) {
                this.setState({
                    errorMessage: "Please complete all the fields before saving. Credits are from 0 to 5 only.", 
                    errorVisibility: true 
                });
            } else {
                this.props.history.push("../../courses");
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Edit Course</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                            
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group className="mb-3" controlId="title">
                                <Form.Label>Course Name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Course Name" 
                                    value={this.state.title}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="credits">
                                <Form.Label>Credits</Form.Label>
                                <Form.Control 
                                    type="number" 
                                    placeholder="Credits" 
                                    value={this.state.credits}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="departmentID">
                                <Form.Label>Department</Form.Label>
                                <Form.Control 
                                    as="select"
                                    value={this.state.departmentID}
                                    onChange={this.handleChange}>
                                    <option value="0">--Select--</option>
                                    {
                                        this.state.departments.map((department, key) => {
                                            return <option key={key} value={department.departmentID}>{ department.name }</option>
                                        }) 
                                    }
                                </Form.Control>
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Update Course
                            </Button>
                        </Form>

                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../delete/${this.state.courseID}`}>Delete</Link>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getCourse: courseOperations.getCourse,
        updateCourse: courseOperations.updateCourse,
        getDepartments: departmentOperations.getDepartments,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(CourseEditScreen)));