import reducer from "./reducers";
import { routes } from "../routes";
import * as courseOperations from "./operations";

export {
    courseOperations
};

export default {
    routes,
    reducer
}
