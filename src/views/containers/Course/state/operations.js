import * as Path from './apiRoutes';
import ApiService from "../../../../utils/apiService";

const getCourses = () => async () => {
    try {
        const response = await ApiService.get(Path.COURSES);
        return response.data;
    } catch (error) {
        return error;
    }
};

const getCourse = (id) => async () => {
    try {
        const response = await ApiService.get(Path.COURSES + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const updateCourse = (id, payload) => async () => {
    try {
        const response = await ApiService.put(Path.COURSES + "/" + id, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

const deleteCourse = (id) => async () => {
    try {
        const response = await ApiService.delete(Path.COURSES + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const createCourse = (payload) => async () => {
    try {
        const response = await ApiService.post(Path.COURSES, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

export {
    getCourses,
    getCourse,
    updateCourse,
    deleteCourse,
    createCourse,
};
