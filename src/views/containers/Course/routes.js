import CourseScreen from './CourseScreen'
import CourseCreateScreen from './CourseCreateScreen'
import CourseDeleteScreen from './CourseDeleteScreen'
import CourseDetailScreen from './CourseDetailScreen'
import CourseEditScreen from './CourseEditScreen'
import { 
    COURSE_ROUTE, 
    COURSE_CREATE_ROUTE, 
    COURSE_DELETE_ROUTE, 
    COURSE_DETAILS_ROUTE, 
    COURSE_EDIT_ROUTE 
} from "../../../config/routes";

export const routes = [
    {
        path: COURSE_ROUTE,
        component: CourseScreen,
        exact: true,
        public: false,
    },
    {
        path: COURSE_CREATE_ROUTE,
        component: CourseCreateScreen,
        exact: true,
        public: false,
    },
    {
        path: COURSE_DELETE_ROUTE,
        component: CourseDeleteScreen,
        exact: true,
        public: false,
    },
    {
        path: COURSE_DETAILS_ROUTE,
        component: CourseDetailScreen,
        exact: true,
        public: false,
    },
    {
        path: COURSE_EDIT_ROUTE,
        component: CourseEditScreen,
        exact: true,
        public: false,
    },
];