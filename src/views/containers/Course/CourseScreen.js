import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Table } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { courseOperations } from './state'
import MessageError from '../../components/MessageError'

class CourseScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            courses: [],
        };
    }
    
    componentDidMount() {
       this.getCourses();
    }

    getCourses() {
        this.props.getCourses().then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ courses: result.response });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Courses</h3>
                        <Link to={`../courses/create`}>Create</Link>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        <Table striped >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Credits</th>
                                    <th>Department</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                    this.state.courses.map((course, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{ course.courseID }</td>
                                                <td>{ course.title }</td>
                                                <td>{ course.credits }</td>
                                                <td>{ course.department.name }</td>
                                                <td>
                                                    <Link to={`../courses/edit/${course.courseID}`}>Edit</Link> | <Link to={`../courses/details/${course.courseID}`}>Details</Link> | <Link to={`../courses/delete/${course.courseID}`}>Delete</Link>
                                                </td>
                                            </tr>)
                                    }) 
                                }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getCourses: courseOperations.getCourses,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(CourseScreen)));