import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { courseOperations } from './state'
import MessageError from '../../components/MessageError'

class CourseDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            courses: {},
            department: {},
        };
    }
    
    componentDidMount() {
       this.getCourse();
    }

    getCourse() {
        const { id } = this.props.match.params;
        this.props.getCourse(id).then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ 
                        courses: result.response,
                        department: result.response.department,
                    });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Details Courses</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        Course #: <b>{ this.state.courses.courseID }</b><br/>
                        Course Name: <b>{ this.state.courses.title }</b><br/>
                        Credits: <b>{ this.state.courses.credits }</b><br/>
                        Department: <b>{ this.state.department.name }</b><br/>

                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../edit/${this.state.courses.courseID}`}>Edit</Link>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getCourse: courseOperations.getCourse,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(CourseDetailScreen)));