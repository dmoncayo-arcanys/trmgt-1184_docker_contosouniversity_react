import reducer from "./reducers";
import { routes } from "../routes";
import * as studentOperations from "./operations";

export {
    studentOperations
};

export default {
    routes,
    reducer
}
