import * as Path from './apiRoutes';
import ApiService from "../../../../utils/apiService";

const getStudents = () => async () => {
    try {
        const response = await ApiService.get(Path.STUDENTS);
        return response.data;
    } catch (error) {
        return error;
    }
};

const getStudent = (id) => async () => {
    try {
        const response = await ApiService.get(Path.STUDENTS + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const updateStudent = (id, payload) => async () => {
    try {
        const response = await ApiService.put(Path.STUDENTS + "/" + id, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

const deleteStudent = (id) => async () => {
    try {
        const response = await ApiService.delete(Path.STUDENTS + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const createStudent = (payload) => async () => {
    try {
        const response = await ApiService.post(Path.STUDENTS, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

export {
    getStudents,
    getStudent,
    updateStudent,
    deleteStudent,
    createStudent,
};
