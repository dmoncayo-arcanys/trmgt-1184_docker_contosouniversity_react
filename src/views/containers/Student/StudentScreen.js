import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Table } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { studentOperations } from './state'
import MessageError from '../../components/MessageError'

class StudentScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            students: [],
        };
    }
    
    componentDidMount() {
       this.getStudents();
    }

    getStudents() {
        this.props.getStudents().then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ students: result.response });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Students</h3>
                        <Link to={`../students/create`}>Create</Link>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        <Table striped >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Enrollment Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                    this.state.students.map((student, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{ student.id }</td>
                                                <td>{ student.firstMidName }</td>
                                                <td>{ student.lastName }</td>
                                                <td>{ student.enrollmentDate }</td>
                                                <td>
                                                    <Link to={`../students/edit/${student.id}`}>Edit</Link> | <Link to={`../students/details/${student.id}`}>Details</Link> | <Link to={`../students/delete/${student.id}`}>Delete</Link>
                                                </td>
                                            </tr>)
                                    }) 
                                }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getStudents: studentOperations.getStudents,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(StudentScreen)));