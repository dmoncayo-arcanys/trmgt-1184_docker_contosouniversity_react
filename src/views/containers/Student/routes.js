import StudentScreen from './StudentScreen'
import StudentCreateScreen from './StudentCreateScreen'
import StudentEditScreen from './StudentEditScreen'
import StudentDetailScreen from './StudentDetailScreen'
import StudentDeleteScreen from './StudentDeleteScreen'
import { 
    STUDENT_ROUTE, 
    STUDENT_CREATE_ROUTE,
    STUDENT_EDIT_ROUTE,
    STUDENT_DETAILS_ROUTE,
    STUDENT_DELETE_ROUTE
} from "../../../config/routes";

export const routes = [
    {
        path: STUDENT_ROUTE,
        component: StudentScreen,
        exact: true,
        public: false,
    },
    {
        path: STUDENT_CREATE_ROUTE,
        component: StudentCreateScreen,
        exact: true,
        public: false,
    },
    {
        path: STUDENT_DETAILS_ROUTE,
        component: StudentDetailScreen,
        exact: true,
        public: false,
    },
    {
        path: STUDENT_EDIT_ROUTE,
        component: StudentEditScreen,
        exact: true,
        public: false,
    },
    {
        path: STUDENT_DELETE_ROUTE,
        component: StudentDeleteScreen,
        exact: true,
        public: false,
    },
];