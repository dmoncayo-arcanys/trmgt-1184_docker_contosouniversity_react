import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { studentOperations } from './state'
import { formatDate } from '../../../utils/utils'
import MessageError from '../../components/MessageError'

class StudentCreateScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            id: 0,
            firstMidName: "",
            lastName: "",
            enrollmentDate: formatDate(),
        };
    }
    
    componentDidMount() {
        
    }

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let { firstMidName, lastName, enrollmentDate } = this.state;
        this.props.createStudent({
            firstMidName: firstMidName,
            lastName: lastName,
            enrollmentDate: enrollmentDate,
        }).then((result) => {
            console.log("result ", result);
            this.props.history.push("../../students");
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Create Students</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group className="mb-3" controlId="firstMidName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="First name" 
                                    value={this.state.firstMidName}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="lastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Last name" 
                                    value={this.state.lastName}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="enrollmentDate">
                                <Form.Label>Enrollment date</Form.Label>
                                <Form.Control 
                                    type="date" 
                                    placeholder="Enrollment date" 
                                    value={this.state.enrollmentDate}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Create Student
                            </Button>
                        </Form>
                        
                        <br/>
                        <Link to={`../`}>Back</Link>

                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        createStudent: studentOperations.createStudent,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(StudentCreateScreen)));