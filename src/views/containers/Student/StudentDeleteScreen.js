import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { studentOperations } from './state'
import MessageError from '../../components/MessageError'

class StudentDeleteScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            student: {},
            courses: [],
        };
    }
    
    componentDidMount() {
        this.getStudent();
    }
 
    getStudent() {
        const { id } = this.props.match.params;
        this.props.getStudent(id).then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ 
                        student: result.response,
                        courses: result.response.enrollments
                    });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }
    
    handleSubmit = () => {
        const { id } = this.props.match.params;
        this.props.deleteStudent(id).then((result) => {
            console.log("result ", result);
            this.props.history.push("../../students");
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Delete Students</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        First Name: <b>{ this.state.student.firstMidName }</b><br/>
                        Last Name: <b>{ this.state.student.lastName }</b><br/>
                        Enrolled Date: <b>{ this.state.student.enrollmentDate }</b><br/>

                        <Button 
                            onClick={() => this.handleSubmit()}
                            variant="danger" 
                            type="submit" 
                            style={{marginTop: 15, marginBottom: 15}}>
                            Delete Student
                        </Button>

                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../edit/${this.state.student.id}`}>Edit</Link>

                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getStudent: studentOperations.getStudent,
        deleteStudent: studentOperations.deleteStudent,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(StudentDeleteScreen)));