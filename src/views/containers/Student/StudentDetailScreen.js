import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Table } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { studentOperations } from './state'
import MessageError from '../../components/MessageError'

class StudentDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            student: {},
            courses: [],
        };
    }
    
    componentDidMount() {
       this.getStudent();
    }

    getStudent() {
        const { id } = this.props.match.params;
        this.props.getStudent(id).then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ 
                        student: result.response,
                        courses: result.response.enrollments
                    });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Details Students</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        First Name: <b>{ this.state.student.firstMidName }</b><br/>
                        Last Name: <b>{ this.state.student.lastName }</b><br/>
                        Enrolled Date: <b>{ this.state.student.enrollmentDate }</b><br/>
                        Enrolled Courses:<br/>

                        <Table striped >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Credits</th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                    this.state.courses.map((course, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{ course.course.courseID }</td>
                                                <td>{ course.course.title }</td>
                                                <td>{ course.course.credits }</td>
                                            </tr>)
                                    }) 
                                }
                            </tbody>
                        </Table>

                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../edit/${this.state.student.id}`}>Edit</Link>

                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getStudent: studentOperations.getStudent,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(StudentDetailScreen)));