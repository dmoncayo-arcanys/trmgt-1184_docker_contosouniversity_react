import React, {Component} from 'react';
import { Card } from 'react-bootstrap';

class Error404 extends Component {
    render(){
        return(
            <Card>
                <center>
                    <h1>
                        404 Page Not Found
                    </h1>
                </center>
            </Card>
        );
    }
}

export default Error404;