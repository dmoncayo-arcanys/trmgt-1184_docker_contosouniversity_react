import MenuScreen from './MenuScreen'
import { ROOT } from "../../../config/settings";

export const routes = [
    {
        path: ROOT,
        component: MenuScreen,
        exact: true,
        public: true,
    },
];