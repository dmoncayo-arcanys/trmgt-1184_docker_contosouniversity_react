import { routes } from "../routes";
import * as menuOperations from "./operations";

export {
    menuOperations
};

export default {
    routes
}
