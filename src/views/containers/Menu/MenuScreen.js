import React, { Component } from "react";
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Container } from "react-bootstrap";
import { bindActionCreators } from 'redux';

class MenuScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "Welcome to Contoso University",
            description: "Testing React, .NET Core Web API, and IdentityServer4",
        };
    }
    
    componentDidMount() {
        console.log("this.props ", this.props)
        if(this.props.location.search !== null && this.props.location.search !== undefined && this.props.location.search !== "") {
            this.setState({
                title: "Authenticating Session ...",
                description: "Please wait while we are authenticating this session"
            });
        }
    }

    render() {
        return (
            <Container>
                <center style={{ marginBottom: 200 }}>
                    <h1>{ this.state.title }</h1>
                    <p>{ this.state.description }</p>
                </center>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {}, 
    dispatch
);
  
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MenuScreen));