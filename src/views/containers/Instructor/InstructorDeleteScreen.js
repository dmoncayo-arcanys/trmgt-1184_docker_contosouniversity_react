import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { instructorOperations } from './state'
import MessageError from '../../components/MessageError'

class InstructorDeleteScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            instructors: {},
        };
    }
    
    componentDidMount() {
        this.getInstructor();
    }
 
    getInstructor() {
         const { id } = this.props.match.params;
         this.props.getInstructor(id).then((result) => {
             console.log("result ", result);
             let message = "";
             if(result !== null) {
                 if(result === 401) {
                     message = "Your session is expired. Please login.";
                     this.props.signOut();
                 } else if(result.response === null || result.response === undefined) {
                     message = "Cannot connect to the database. Please check your connection or try again later.";
                 } else {
                     this.setState({ 
                         instructors: result.response,
                         courseAssignments: result.response.courseAssignments,
                     });
                 }
             } else {
                 message = "Cannot connect to the database. Please check your connection or try again later.";
             }
             if(message !== "") {
                 this.setState({
                     errorMessage: message, 
                     errorVisibility: true 
                 });
             }
         });
    }
    
    handleSubmit = () => {
        const { id } = this.props.match.params;
        this.props.deleteInstructor(id).then((result) => {
            console.log("result ", result);
            this.props.history.push("../../instructors");
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Delete Instructors</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        First Name: <b>{ this.state.instructors.firstMidName }</b><br/>
                        Last Name: <b>{ this.state.instructors.lastName }</b><br/>
                        Enrolled Date: <b>{ this.state.instructors.hireDate }</b><br/>

                        <Button 
                            onClick={() => this.handleSubmit()}
                            variant="danger" 
                            type="submit" 
                            style={{marginTop: 15, marginBottom: 15}}>
                            Delete Instructor
                        </Button>

                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../edit/${this.state.instructors.id}`}>Edit</Link>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getInstructor: instructorOperations.getInstructor,
        deleteInstructor: instructorOperations.deleteInstructor,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(InstructorDeleteScreen)));