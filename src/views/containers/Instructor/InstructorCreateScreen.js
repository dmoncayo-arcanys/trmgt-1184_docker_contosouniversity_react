import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { instructorOperations } from './state'
import { formatDate } from '../../../utils/utils'
import MessageError from '../../components/MessageError'

class InstructorCreateScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            id: 0,
            firstMidName: "",
            lastName: "",
            hireDate: formatDate(),
        };
    }
    
    componentDidMount() {
        
    }

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { id } = this.props.match.params;
        let { firstMidName, lastName, hireDate } = this.state;
        this.props.createInstructor({
            firstMidName: firstMidName,
            lastName: lastName,
            hireDate: hireDate,
        }).then((result) => {
            console.log("result ", result);
            this.props.history.push("../../instructors");
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Create Instructors</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group className="mb-3" controlId="firstMidName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="First name" 
                                    value={this.state.firstMidName}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="lastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Last name" 
                                    value={this.state.lastName}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="hireDate">
                                <Form.Label>Enrollment date</Form.Label>
                                <Form.Control 
                                    type="date" 
                                    placeholder="Hire date" 
                                    value={this.state.hireDate}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Update Instructor
                            </Button>
                        </Form>
                        
                        <br/>
                        <Link to={`../`}>Back</Link>

                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        createInstructor: instructorOperations.createInstructor,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(InstructorCreateScreen)));