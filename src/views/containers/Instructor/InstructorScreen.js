import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Table } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { instructorOperations } from './state'
import MessageError from '../../components/MessageError'

class InstructorScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            instructors: [],
        };
    }
    
    componentDidMount() {
       this.getInstructors();
    }

    getInstructors() {
        this.props.getInstructors().then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ instructors: result.response });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Instructors</h3>
                        <Link to={`../instructors/create`}>Create</Link>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        <Table striped >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Hire Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                    this.state.instructors.map((instructor, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{ instructor.id }</td>
                                                <td>{ instructor.firstMidName }</td>
                                                <td>{ instructor.lastName }</td>
                                                <td>{ instructor.hireDate }</td>
                                                <td>
                                                    <Link to={`../instructors/edit/${instructor.id}`}>Edit</Link> | <Link to={`../instructors/details/${instructor.id}`}>Details</Link> | <Link to={`../instructors/delete/${instructor.id}`}>Delete</Link>
                                                </td>
                                            </tr>)
                                    }) 
                                }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getInstructors: instructorOperations.getInstructors,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(InstructorScreen)));