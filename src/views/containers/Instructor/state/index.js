import reducer from "./reducers";
import { routes } from "../routes";
import * as instructorOperations from "./operations";

export {
    instructorOperations
};

export default {
    routes,
    reducer
}
