import * as Path from './apiRoutes';
import ApiService from "../../../../utils/apiService";

const getInstructors = () => async () => {
    try {
        const response = await ApiService.get(Path.INSTRUCTORS);
        return response.data;
    } catch (error) {
        return error;
    }
};

const getInstructor = (id) => async () => {
    try {
        const response = await ApiService.get(Path.INSTRUCTORS + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const updateInstructor = (id, payload) => async () => {
    try {
        const response = await ApiService.put(Path.INSTRUCTORS + "/" + id, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

const deleteInstructor = (id) => async () => {
    try {
        const response = await ApiService.delete(Path.INSTRUCTORS + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const createInstructor = (payload) => async () => {
    try {
        const response = await ApiService.post(Path.INSTRUCTORS, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

export {
    getInstructors,
    getInstructor,
    updateInstructor,
    deleteInstructor,
    createInstructor,
};
