import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { instructorOperations } from './state'
import { formatDate } from '../../../utils/utils'
import MessageError from '../../components/MessageError'

class InstructorEditScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            id: 0,
            firstMidName: "",
            lastName: "",
            hireDate: formatDate(),
        };
    }
    
    componentDidMount() {
        this.getInstructor();
    }
 
    getInstructor() {
         const { id } = this.props.match.params;
         this.props.getInstructor(id).then((result) => {
             console.log("result ", result);
             let message = "";
             if(result !== null) {
                 if(result === 401) {
                     message = "Your session is expired. Please login.";
                     this.props.signOut();
                 } else if(result.response === null || result.response === undefined) {
                     message = "Cannot connect to the database. Please check your connection or try again later.";
                 } else {
                    this.setState({ 
                        id: result.response.id,
                        firstMidName: result.response.firstMidName,
                        lastName: result.response.lastName,
                        hireDate: formatDate(new Date(result.response.hireDate)),
                    });
                 }
             } else {
                 message = "Cannot connect to the database. Please check your connection or try again later.";
             }
             if(message !== "") {
                 this.setState({
                     errorMessage: message, 
                     errorVisibility: true 
                 });
             }
         });
    }

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { id } = this.props.match.params;
        let { firstMidName, lastName, hireDate } = this.state;
        this.props.updateInstructor(id, {
            instructors: {
                id: id,
                firstMidName: firstMidName,
                lastName: lastName,
                hireDate: hireDate,
            },
            selectedCourses: []
        }).then((result) => {
            console.log("result ", result);
            this.props.history.push("../../instructors");
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Edit Instructors</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group className="mb-3" controlId="firstMidName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="First name" 
                                    value={this.state.firstMidName}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="lastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Last name" 
                                    value={this.state.lastName}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="hireDate">
                                <Form.Label>Enrollment date</Form.Label>
                                <Form.Control 
                                    type="date" 
                                    placeholder="Hire date" 
                                    value={this.state.hireDate}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Update Instructor
                            </Button>
                        </Form>
                        
                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../delete/${this.state.id}`}>Delete</Link>

                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getInstructor: instructorOperations.getInstructor,
        updateInstructor: instructorOperations.updateInstructor,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(InstructorEditScreen)));