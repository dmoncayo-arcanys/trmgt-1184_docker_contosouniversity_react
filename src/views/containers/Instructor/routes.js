import InstructorScreen from './InstructorScreen'
import InstructorCreateScreen from './InstructorCreateScreen'
import InstructorEditScreen from './InstructorEditScreen'
import InstructorDetailScreen from './InstructorDetailScreen'
import InstructorDeleteScreen from './InstructorDeleteScreen'
import { 
    INSTRUCTOR_ROUTE,
    INSTRUCTOR_CREATE_ROUTE,
    INSTRUCTOR_EDIT_ROUTE,
    INSTRUCTOR_DETAILS_ROUTE,
    INSTRUCTOR_DELETE_ROUTE,
} from "../../../config/routes";

export const routes = [
    {
        path: INSTRUCTOR_ROUTE,
        component: InstructorScreen,
        exact: true,
        public: false,
    },
    {
        path: INSTRUCTOR_CREATE_ROUTE,
        component: InstructorCreateScreen,
        exact: true,
        public: false,
    },
    {
        path: INSTRUCTOR_EDIT_ROUTE,
        component: InstructorEditScreen,
        exact: true,
        public: false,
    },
    {
        path: INSTRUCTOR_DETAILS_ROUTE,
        component: InstructorDetailScreen,
        exact: true,
        public: false,
    },
    {
        path: INSTRUCTOR_DELETE_ROUTE,
        component: InstructorDeleteScreen,
        exact: true,
        public: false,
    },
];