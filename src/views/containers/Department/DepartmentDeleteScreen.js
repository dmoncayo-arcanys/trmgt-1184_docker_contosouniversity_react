import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { departmentOperations } from './state'
import MessageError from '../../components/MessageError'

class DepartmentDeleteScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            departments: {},
        };
    }
    
    componentDidMount() {
        this.getDepartment();
    }
 
    getDepartment() {
         const { id } = this.props.match.params;
         this.props.getDepartment(id).then((result) => {
             console.log("result ", result);
             let message = "";
             if(result !== null) {
                 if(result === 401) {
                     message = "Your session is expired. Please login.";
                     this.props.signOut();
                 } else if(result.response === null || result.response === undefined) {
                     message = "Cannot connect to the database. Please check your connection or try again later.";
                 } else {
                     this.setState({ 
                         departments: result.response,
                         administrator: result.response.administrator,
                     });
                 }
             } else {
                 message = "Cannot connect to the database. Please check your connection or try again later.";
             }
             if(message !== "") {
                 this.setState({
                     errorMessage: message, 
                     errorVisibility: true 
                 });
             }
         });
    }

    handleSubmit = () => {
        const { id } = this.props.match.params;
        this.props.deleteDepartment(id).then((result) => {
            console.log("result ", result);
            this.props.history.push("../../departments");
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Delete Departments</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        Department #: <b>{ this.state.departments.departmentID }</b><br/>
                        Department Name: <b>{ this.state.departments.name }</b><br/>
                        Budget: <b>{ this.state.departments.budget }</b><br/>

                        <Button 
                            onClick={() => this.handleSubmit()}
                            variant="danger" 
                            type="submit" 
                            style={{marginTop: 15, marginBottom: 15}}>
                            Delete Department
                        </Button>

                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../edit/${this.state.departments.departmentID}`}>Edit</Link>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getDepartment: departmentOperations.getDepartment,
        deleteDepartment: departmentOperations.deleteDepartment,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(DepartmentDeleteScreen)));