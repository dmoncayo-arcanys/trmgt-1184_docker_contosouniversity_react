import reducer from "./reducers";
import { routes } from "../routes";
import * as departmentOperations from "./operations";

export {
    departmentOperations
};

export default {
    routes,
    reducer
}
