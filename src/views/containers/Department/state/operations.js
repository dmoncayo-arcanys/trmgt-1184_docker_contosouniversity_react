import * as Path from './apiRoutes';
import ApiService from "../../../../utils/apiService";

const getDepartments = () => async () => {
    try {
        const response = await ApiService.get(Path.DEPARTMENTS);
        return response.data;
    } catch (error) {
        return error;
    }
};

const getDepartment = (id) => async () => {
    try {
        const response = await ApiService.get(Path.DEPARTMENTS + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const updateDepartment = (id, payload) => async () => {
    try {
        const response = await ApiService.put(Path.DEPARTMENTS + "/" + id, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

const deleteDepartment = (id) => async () => {
    try {
        const response = await ApiService.delete(Path.DEPARTMENTS + "/" + id);
        return response.data;
    } catch (error) {
        return error;
    }
};

const createDepartment = (payload) => async () => {
    try {
        const response = await ApiService.post(Path.DEPARTMENTS, payload);
        return response.data;
    } catch (error) {
        return error;
    }
};

export {
    getDepartments,
    getDepartment,
    updateDepartment,
    deleteDepartment,
    createDepartment
};
