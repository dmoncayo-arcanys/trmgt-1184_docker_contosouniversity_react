import DepartmentScreen from './DepartmentScreen'
import DepartmentCreateScreen from './DepartmentCreateScreen'
import DepartmentEditScreen from './DepartmentEditScreen'
import DepartmentDetailScreen from './DepartmentDetailScreen'
import DepartmentDeleteScreen from './DepartmentDeleteScreen'
import { 
    DEPARTMENT_ROUTE,
    DEPARTMENT_CREATE_ROUTE,
    DEPARTMENT_EDIT_ROUTE,
    DEPARTMENT_DETAILS_ROUTE,
    DEPARTMENT_DELETE_ROUTE,
} from "../../../config/routes";

export const routes = [
    {
        path: DEPARTMENT_ROUTE,
        component: DepartmentScreen,
        exact: true,
        public: false,
    },
    {
        path: DEPARTMENT_CREATE_ROUTE,
        component: DepartmentCreateScreen,
        exact: true,
        public: false,
    },
    {
        path: DEPARTMENT_EDIT_ROUTE,
        component: DepartmentEditScreen,
        exact: true,
        public: false,
    },
    {
        path: DEPARTMENT_DETAILS_ROUTE,
        component: DepartmentDetailScreen,
        exact: true,
        public: false,
    },
    {
        path: DEPARTMENT_DELETE_ROUTE,
        component: DepartmentDeleteScreen,
        exact: true,
        public: false,
    },
];