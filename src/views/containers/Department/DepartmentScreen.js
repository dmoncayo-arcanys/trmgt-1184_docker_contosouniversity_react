import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Table } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { departmentOperations } from './state'
import MessageError from '../../components/MessageError'

class DepartmentScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            departments: [],
        };
    }
    
    componentDidMount() {
       this.getDepartments();
    }

    getDepartments() {
        this.props.getDepartments().then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ departments: result.response });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Departments</h3>
                        <Link to={`../departments/create`}>Create</Link>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        <Table striped >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Department Name</th>
                                    <th>Budget</th>
                                    <th>Instructor</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                    this.state.departments.map((department, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{ department.departmentID }</td>
                                                <td>{ department.name }</td>
                                                <td>{ department.budget }</td>
                                                <td>{ department.administrator.fullName }</td>
                                                <td>
                                                    <Link to={`../departments/edit/${department.departmentID}`}>Edit</Link> | <Link to={`../departments/details/${department.departmentID}`}>Details</Link> | <Link to={`../departments/delete/${department.departmentID}`}>Delete</Link>
                                                </td>
                                            </tr>)
                                    }) 
                                }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getDepartments: departmentOperations.getDepartments,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(DepartmentScreen)));