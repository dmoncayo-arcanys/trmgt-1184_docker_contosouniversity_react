import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { departmentOperations } from './state'
import MessageError from '../../components/MessageError'

class DepartmentDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            departments: {},
            administrator: {},
        };
    }
    
    componentDidMount() {
       this.getDepartment();
    }

    getDepartment() {
        const { id } = this.props.match.params;
        this.props.getDepartment(id).then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ 
                        departments: result.response,
                        administrator: result.response.administrator,
                    });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Details Departments</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        Department #: <b>{ this.state.departments.departmentID }</b><br/>
                        Department Name: <b>{ this.state.departments.name }</b><br/>
                        Budget: <b>{ this.state.departments.budget }</b><br/>
                        Administrator: <b>{ this.state.administrator.fullName }</b><br/>
                        
                        <br/>
                        <Link to={`../`}>Back</Link> | <Link to={`../edit/${this.state.departments.departmentID}`}>Edit</Link>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        getDepartment: departmentOperations.getDepartment,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(DepartmentDetailScreen)));