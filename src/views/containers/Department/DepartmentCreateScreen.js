import React, { Component } from "react";
import { connect } from 'react-redux';
import { withAuth } from 'oidc-react';
import { withRouter, Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { bindActionCreators } from 'redux';
import { departmentOperations } from './state'
import { instructorOperations } from '../Instructor/state'
import MessageError from '../../components/MessageError'

class DepartmentCreateScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorVisibility: false,
            departmentID: 0,
            name: "",
            budget: 0,
            instructorID: 0,
            instructors: [],
        };
    }

    componentDidMount() {
        this.getInstructors();
    }

    getInstructors() {
        this.props.getInstructors().then((result) => {
            console.log("result ", result);
            let message = "";
            if(result !== null) {
                if(result === 401) {
                    message = "Your session is expired. Please login.";
                    this.props.signOut();
                } else if(result.response === null || result.response === undefined) {
                    message = "Cannot connect to the database. Please check your connection or try again later.";
                } else {
                    this.setState({ instructors: result.response });
                }
            } else {
                message = "Cannot connect to the database. Please check your connection or try again later.";
            }
            if(message !== "") {
                this.setState({
                    errorMessage: message, 
                    errorVisibility: true 
                });
            }
        });
    }

    

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let message = "";
        let { name, budget, instructorID } = this.state;
        if(name !== "" && instructorID > 0) {
            this.props.createDepartment({
                name: name,
                budget: budget,
                instructorID: instructorID,
            }).then((result) => {
                console.log("result ", result);
                this.props.history.push("../../departments");
            });
        } else {
            message = "Please complete all the fields before saving.";
        }
        if(message !== "") {
            this.setState({
                errorMessage: message, 
                errorVisibility: true 
            });
        }
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Create Departments</h3>
                        <br/>
                        <br/>

                        <MessageError 
                            message={this.state.errorMessage} 
                            visible={this.state.errorVisibility} />
                        
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Department name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="First name" 
                                    value={this.state.name}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="budget">
                                <Form.Label>Budget</Form.Label>
                                <Form.Control 
                                    type="number" 
                                    placeholder="Budget" 
                                    value={this.state.budget}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="instructorID">
                                <Form.Label>Department</Form.Label>
                                <Form.Control 
                                    as="select"
                                    value={this.state.instructorID}
                                    onChange={this.handleChange}>
                                    <option value="0">--Select--</option>
                                    {
                                        this.state.instructors.map((instructor, key) => {
                                            return <option key={key} value={instructor.id}>{ instructor.fullName }</option>
                                        }) 
                                    }
                                </Form.Control>
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Create Department
                            </Button>
                        </Form>

                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state
    };
};
  
const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        createDepartment: departmentOperations.createDepartment,
        getInstructors: instructorOperations.getInstructors,
    }, 
    dispatch
);
  
export default withAuth(withRouter(connect(mapStateToProps, mapDispatchToProps)(DepartmentCreateScreen)));